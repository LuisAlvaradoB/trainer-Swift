//: Playground - noun: a place where people can play

import UIKit

var nombres = ["Deyanira", "Lucy", "Hugo", "Elizabetha", "David", "Laura", "Angelica", "Rafael", "Víctor", "Luis"]

for var i = 0; i < nombres.count; i++ {
    print("\(i)\t\(nombres[i])")
}

for n in nombres {
    print("\(n)")
}
