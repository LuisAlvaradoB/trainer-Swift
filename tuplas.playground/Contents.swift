//: Playground - noun: a place where people can play

import UIKit

func calcularIndiceDeMasaCorporal ( pesoIntegral peso : Double, altura : Double) -> ( Double, String ) {
    
    let imc = peso / ( altura * altura )
    var mensaje = ""
    
    if ( imc > 18.00 && imc < 25.00) {
        mensaje = "Peso normal"
    } else {
        mensaje = "Debes acudir con tu médico"
    }
    let resultado = ( imc, mensaje )
    return resultado
}

    let resultadoIMC = calcularIndiceDeMasaCorporal(pesoIntegral: 60.0, altura: 1.6)