//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*
 Las tijeras cortan el papel
 el papel cubre a la piedra
 la piedra aplasta al lagarto
 el lagarto envenena a Spock
 Spock destroza las tijeras
 las tijeras decapitan al lagarto
 el lagarto se come el papel
 el papel refuta a Spock
 Spock vaporiza la piedra,
 Piedra aplasta las tijeras.
 */

//: Piedra Papel Tijera Lagarto Spock


let MAX : UInt32 = 4
let MIN : UInt32 = 0
func numeroAleatorio()
{
    var random_number = Int(arc4random_uniform(MAX) + MIN)
    print ("random = ", random_number);
}

numeroAleatorio()


var campo = [ ["Empate","Perdiste","Ganaste","Ganaste","Perdiste"  ],
              ["Ganaste","Empate","Perdiste","Perdiste","Perdiste" ],
              ["Perdiste","Ganaste","Empate","Ganaste","Perdiste"  ],
              ["Perdiste","Ganaste","Perdiste","Empate","Ganaste"  ],
              ["Ganaste","Perdiste","Ganaste","Perdiste","Empate"  ] ]
print (campo [1][1])

